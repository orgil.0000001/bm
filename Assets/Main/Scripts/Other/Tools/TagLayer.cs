namespace Orgil {
	public enum Tag { Untagged, Respawn, Finish, EditorOnly, MainCamera, Player, GameController, Box, Bot, PickUp, Wall }
	public class Lyr { public const int Default = 0, TransparentFX = 1, IgnoreRaycast = 2, Player = 3, Water = 4, UI = 5, Wall = 6, Bot = 7, Bomb = 8, Box = 9; }
	public class Lm { public const int Default = 1, TransparentFX = 2, IgnoreRaycast = 4, Player = 8, Water = 16, UI = 32, Wall = 64, Bot = 128, Bomb = 256, Box = 512; }
}