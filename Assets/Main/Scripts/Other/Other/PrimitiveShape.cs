using System.Collections.Generic;
using UnityEngine;
using Orgil;

public enum PrimitiveShapeTp {
    Plane, Circle,
    Box, BoxIn, BoxHole, BoxHoles, BoxRound,
    Cone, Cylinder, CylinderRound, Capsule, Pipe, PipeRound, PipeRound2, Torus,
    UvSphere, Sphere, HemiSphere, Sector, Segment,
    Spring, SpringRound, Spiral, SpiralRound,
    Pyramid, DiPyramid, ChessPawn, Cup, Star, Star2, Stair, Twist,
    RotModel,
}

[ExecuteInEditMode]
[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class PrimitiveShape : Mb {
    public List<Vector3> roadPnts = new List<Vector3>() { V3.O, V3.f };
    public List<Vector2> roadDatas = new List<Vector2>() { V2.l, V2.np, V2.pp, V2.r };
    public bool roadIsLoop;
    public string roadFc;
    public RoadCrtDirTp roadCrtDirTp;
    public PrimitiveShapeTp type;
    public float tilingY = 1, staAng = 0;
    public bool isCw, isVerNor = false, isUvFull = false;
    public UvTp segUt, ringUt;
    public Tf t;
    public float uvAng = 0;
    [Header("Plane")]
    public int planeSeg = 4;
    public int planeSegCor = 0;
    public float planeR = 0.5f, planeRCor = 0.1f;
    [Header("Pipe")]
    public int pipeSeg = 20;
    public int pipeSegCor = 0, pipeRingCor = 0;
    public float pipeRO = 1, pipeRI = 0.7f, pipeRCor = 0.1f, pipeH = 1;
    [Header("Cup")]
    public int cupSeg = 4;
    public int cupSegCor = 0, cupRingCorDO = 0, cupRingCorU = 0, cupRingCorDI = 0;
    public float cupRO = 1, cupRI = 0.7f, cupRCorDO = 0.1f, cupRCorU = 0.1f, cupRCorDI = 0.1f, cupHO = 2, cupHI = 1.7f;
    [Header("Cylinder")]
    public int cylinderSeg = 20;
    public int cylinderSegCor = 0, cylinderRingCor = 0;
    public float cylinderRD = 0.5f, cylinderRU = 0.5f, cylinderRCor = 0.1f, cylinderH = 1;
    [Header("Circle")]
    public int circleSegments = 20;
    public float circleRadius = 0.5f;
    [Header("Box")]
    public Vector3 boxSize = V3.I;
    [Header("Box In")]
    public Vector3 boxInSize = V3.I;
    public Vector3 boxInSizeIn = V3.V(0.5f);
    public Vector2 boxInPos = V2.O;
    [Header("Box Hole")]
    public Vector3 boxHoleSize = V3.I;
    public Vector2 boxHoleSizeIn = V2.V(0.5f);
    public Vector2 boxHolePos = V2.O;
    [Header("Box Holes")]
    public Vector3 boxHolesSize = V3.I;
    public Vector3 boxHolesSizeIn = V3.V(0.5f);
    public Vector3 boxHolesPos = V3.O;
    [Header("Box Round")]
    public int boxRoundCornerSegments = 10;
    public int boxRoundCornerRings = 10;
    public float boxRoundCornerRadius = 0.1f;
    public Vector3 boxRoundSize = V3.I;
    [Header("Cone")]
    public int coneSegments = 30;
    public float coneRadiusDown = 0.5f;
    public float coneRadiusUp = 0.25f;
    public float coneHeight = 1;
    [Header("Star")]
    public int starSegments = 5;
    public float starRadiusDown = 0.5f;
    public float starRadiusDown2 = 0.3f;
    public float starRadiusUp = 0.5f;
    public float starRadiusUp2 = 0.3f;
    public float starHeight = 1;
    public float starHeightUp = 0;
    public float starHeightDown = 0;
    [Header("Star2")]
    public int star2Segments = 5;
    public float star2Radius = 0.5f;
    public float star2Radius2 = 0.3f;
    public float star2HeightUp = 0.2f;
    public float star2HeightDown = 0.2f;
    [Header("Cylinder Round")]
    public int cylinderRoundSegments = 30;
    public int cylinderRoundRings = 10;
    public float cylinderRoundRadius = 0.5f;
    public float cylinderRoundCornerRadius = 0.1f;
    public float cylinderRoundHeight = 2;
    [Header("Capsule")]
    public int capsuleSegments = 30;
    public int capsuleRings = 20;
    public float capsuleRadius = 0.5f;
    public float capsuleHeight = 2;
    [Header("UV Sphere")]
    public int uvSphereSegments = 30;
    public int uvSphereRings = 20;
    public float uvSphereRadius = 0.5f;
    [Header("Sphere")]
    public SphereTp sphereType;
    public int sphereSubdivisions = 1;
    public float sphereRadius = 0.5f;
    [Header("Torus")]
    public int torusSegments = 30;
    public int torusRings = 20;
    public float torusRadius = 0.4f;
    public float torusCornerRadius = 0.1f;
    [Header("Sector")]
    public int sectorSegments = 20;
    public float sectorRadius = 0.5f;
    public float sectorHeight = 1;
    public int sectorCorSeg = 0, sectorCorRing = 0;
    public float sectorCorSegR = 0.1f, sectorCorRingR = 0.1f;
    public float sectorStartAngle = 0;
    public float sectorAngle = 90;
    [Header("Segment")]
    public int segmentSegments = 20;
    public float segmentRadius = 0.5f;
    public float segmentHeight = 1;
    public int segmentCorSeg = 0, segmentCorRing = 0;
    public float segmentCorSegR = 0.1f, segmentCorRingR = 0.1f;
    public float segmentStartAngle = 0;
    public float segmentAngle = 90;
    [Header("Pipe Round")]
    public int pipeRoundSegments = 30;
    public int pipeRoundRings = 10;
    public float pipeRoundRadiusOut = 0.5f;
    public float pipeRoundRadiusIn = 0.25f;
    public float pipeRoundCornerRadius = 0.1f;
    public float pipeRoundHeight = 1;
    [Header("Pipe Round 2")]
    public int pipeRound2Segments = 30;
    public int pipeRound2Rings = 20;
    public float pipeRound2Radius = 0.5f;
    public float pipeRound2CornerRadius = 0.1f;
    public float pipeRound2Height = 1;
    [Header("Pyramid")]
    public int pyramidSegments = 30;
    public float pyramidRadius = 0.5f;
    public float pyramidHeight = 1;
    [Header("Di Pyramid")]
    public int diPyramidSegments = 30;
    public float diPyramidRadius = 0.5f;
    public float diPyramidHeight = 1;
    [Header("Hemi Sphere")]
    public int hemiSphereSegments = 30;
    public int hemiSphereRings = 20;
    public float hemiSphereRadius = 0.5f;
    [Header("Chess Pawn")]
    public int chessPawnSegments = 30;
    public float chessPawnRadius = 0.3f;
    public float chessPawnHeight = 1;
    [Header("Spring")]
    public int springSegments = 20;
    public float springTubeWidth = 0.1f;
    public float springTubeHeight = 0.1f;
    public float springN = 5;
    public float springRadius = 0.9f;
    public float springSpace = 1;
    public bool springIsRightHand = true;
    [Header("Spring Round")]
    public int springRoundSegments = 20;
    public int springRoundRings = 20;
    public int springRoundTubeRings = 20;
    public float springRoundN = 5;
    public float springRoundRadius = 0.9f;
    public float springRoundTubeRadius = 0.1f;
    public float springRoundSpace = 1;
    public bool springRoundIsRightHand = true;
    [Header("Stair")]
    public Vector3 stairSize = V3.V(2, 5, 5);
    public int stairN = 10;
    [Header("Twist")]
    public int twistSegments = 20;
    public int twistRings = 20;
    public float twistRadiusDown = 0.5f;
    public float twistRadiusUp = 0.5f;
    public float twistHeight = 2;
    public float twistRotateN = 10;
    [Header("Spiral")]
    public int spiralSegments = 30;
    public float spiralN = 5;
    public float spiralRadiusIn = 1;
    public float spiralRadius = 5;
    public float spiralTubeWidth = 0.5f;
    public float spiralTubeHeight = 1;
    public float spiralHeight = 1;
    public bool spiralIsRightHand = true;
    [Header("SpiralRound")]
    public int spiralRoundSegments = 30;
    public int spiralRoundRings = 30;
    public int spiralRoundTubeRings = 10;
    public float spiralRoundN = 5;
    public float spiralRoundRadiusIn = 1;
    public float spiralRoundRadius = 5;
    public float spiralRoundRadiusTube = 0.25f;
    public float spiralRoundHeight = 0;
    public bool spiralRoundIsRightHand = true;
    [Header("Rotation Model")]
    public int rotModelSegments = 30;
    public int rotModelRings = 30;
    public float rotModelRadius = 1;
    public float rotModelHeight = 2;
    public AnimationCurve rotModelAc;
    [HideInInspector]
    public Mesh mesh;
    private void Start() { }
    private void Update() {
        Msh.Init(ref mesh, go);
        switch (type) {
            case PrimitiveShapeTp.Pipe: Msh.Pipe(ref mesh, pipeSeg, pipeSegCor, pipeRingCor, pipeRO, pipeRI, pipeRCor, pipeH, segUt, ringUt, isVerNor, isUvFull, uvAng); break;
            case PrimitiveShapeTp.Cup: Msh.Cup(ref mesh, cupSeg, cupSegCor, cupRingCorDO, cupRingCorU, cupRingCorDI, cupRO, cupRI, cupRCorDO, cupRCorU, cupRCorDI, cupHO, cupHI, segUt, ringUt, isVerNor, isUvFull, uvAng); break;
            case PrimitiveShapeTp.Cylinder: Msh.Cylinder(ref mesh, cylinderSeg, cylinderSegCor, cylinderRingCor, cylinderRD, cylinderRU, cylinderRCor, cylinderH, segUt, ringUt, isVerNor, isUvFull, uvAng); break;
            case PrimitiveShapeTp.Plane: Msh.Plane(ref mesh, planeSeg, planeSegCor, planeR, planeRCor, segUt, ringUt, uvAng); break;
            case PrimitiveShapeTp.Circle: Msh.Plane(ref mesh, circleSegments, planeSegCor, circleRadius, planeRCor, segUt, ringUt, uvAng); break;
            case PrimitiveShapeTp.Box: Msh.Box(ref mesh, boxSize); break;
            case PrimitiveShapeTp.BoxIn: Msh.BoxIn(ref mesh, boxInSize, boxInSizeIn, boxInPos); break;
            case PrimitiveShapeTp.BoxHole: Msh.BoxHole(ref mesh, boxHoleSize, boxHoleSizeIn, boxHolePos); break;
            case PrimitiveShapeTp.BoxHoles: Msh.BoxHoles(ref mesh, boxHolesSize, boxHolesSizeIn, boxHolesPos); break;
            case PrimitiveShapeTp.BoxRound: Msh.BoxRound(ref mesh, boxRoundCornerSegments, boxRoundCornerRings, boxRoundCornerRadius, boxRoundSize, isVerNor); break;
            case PrimitiveShapeTp.Cone: Msh.Cylinder(ref mesh, coneSegments, coneRadiusDown, coneRadiusUp, coneHeight, isVerNor, isUvFull); break;
            case PrimitiveShapeTp.CylinderRound: Msh.CylinderRound(ref mesh, cylinderRoundSegments, cylinderRoundRings, cylinderRoundRadius, cylinderRoundCornerRadius, cylinderRoundHeight, isVerNor, isUvFull); break;
            case PrimitiveShapeTp.Capsule: Msh.Capsule(ref mesh, capsuleSegments, capsuleRings, capsuleRadius, capsuleHeight, isVerNor, isUvFull); break;
            case PrimitiveShapeTp.UvSphere: Msh.UvSphere(ref mesh, uvSphereSegments, uvSphereRings, uvSphereRadius, isVerNor, isUvFull); break;
            case PrimitiveShapeTp.Sphere: Msh.Sphere(ref mesh, sphereType, sphereSubdivisions, sphereRadius, isVerNor); break;
            case PrimitiveShapeTp.Torus: Msh.Torus(ref mesh, torusSegments, torusRings, torusRadius, torusCornerRadius, isVerNor, isUvFull); break;
            case PrimitiveShapeTp.Sector: Msh.SectorSegment(ref mesh, sectorRadius, sectorHeight, sectorStartAngle, sectorAngle, sectorSegments, sectorCorSeg, sectorCorRing, sectorCorSegR, sectorCorRingR, true, isVerNor); break;
            case PrimitiveShapeTp.Segment: Msh.SectorSegment(ref mesh, segmentRadius, segmentHeight, segmentStartAngle, segmentAngle, segmentSegments, segmentCorSeg, segmentCorRing, segmentCorSegR, segmentCorRingR, false, isVerNor); break;
            case PrimitiveShapeTp.PipeRound: Msh.PipeRound(ref mesh, pipeRoundSegments, pipeRoundRings, pipeRoundRadiusOut, pipeRoundRadiusIn, pipeRoundCornerRadius, pipeRoundHeight, isVerNor, isUvFull); break;
            case PrimitiveShapeTp.PipeRound2: Msh.PipeRound2(ref mesh, pipeRound2Segments, pipeRound2Rings, pipeRound2Radius, pipeRound2CornerRadius, pipeRound2Height, isVerNor, isUvFull); break;
            case PrimitiveShapeTp.Pyramid: Msh.Pyramid(ref mesh, pyramidSegments, pyramidRadius, pyramidHeight, isVerNor, isUvFull); break;
            case PrimitiveShapeTp.DiPyramid: Msh.DiPyramid(ref mesh, diPyramidSegments, diPyramidRadius, diPyramidHeight, isVerNor, isUvFull); break;
            case PrimitiveShapeTp.HemiSphere: Msh.HemiSphere(ref mesh, hemiSphereSegments, hemiSphereRings, hemiSphereRadius, isVerNor, isUvFull); break;
            case PrimitiveShapeTp.ChessPawn: Msh.ChessPawn(ref mesh, chessPawnSegments, chessPawnRadius, chessPawnHeight, isVerNor, isUvFull); break;
            case PrimitiveShapeTp.Spring: Msh.Spiral(ref mesh, roadDatas, springSegments, springN, springRadius, springRadius, springSpace / springSegments * M.CeilI(springN * springSegments), springIsRightHand, isVerNor, 1); break;
            case PrimitiveShapeTp.SpringRound: Msh.SpiralRound(ref mesh, staAng, isCw, springRoundSegments, springRoundRings, springRoundTubeRings, springRoundN, springRoundRadius, springRoundRadius, springRoundTubeRadius, springRoundSpace / springRoundSegments * M.CeilI(springRoundN * springRoundSegments), springRoundIsRightHand, isVerNor, tilingY); break;
            case PrimitiveShapeTp.Star: Msh.Star(ref mesh, starSegments, starRadiusDown, starRadiusDown2, starRadiusUp, starRadiusUp2, starHeight, starHeightUp, starHeightDown); break;
            case PrimitiveShapeTp.Star2: Msh.Star2(ref mesh, star2Segments, star2Radius, star2Radius2, star2HeightUp, star2HeightDown); break;
            case PrimitiveShapeTp.Stair: Msh.Stair(ref mesh, stairSize, stairN); break;
            case PrimitiveShapeTp.Twist: Msh.Twist(ref mesh, twistSegments, twistRings, twistRadiusDown, twistRadiusUp, twistHeight, twistRotateN, isVerNor); break;
            case PrimitiveShapeTp.Spiral: Msh.Spiral(ref mesh, roadDatas, spiralSegments, spiralN, spiralRadiusIn, spiralRadius, spiralHeight, spiralIsRightHand, isVerNor, 1); break;
            case PrimitiveShapeTp.SpiralRound: Msh.SpiralRound(ref mesh, staAng, isCw, spiralRoundSegments, spiralRoundRings, spiralRoundTubeRings, spiralRoundN, spiralRoundRadiusIn, spiralRoundRadius, spiralRoundRadiusTube, spiralRoundHeight, spiralRoundIsRightHand, isVerNor, tilingY); break;
            case PrimitiveShapeTp.RotModel: Msh.RotModel(ref mesh, rotModelSegments, rotModelRings, rotModelRadius, rotModelHeight, rotModelAc, isVerNor, isUvFull); break;
        }
        Msh.UpdTf(ref mesh, t);
    }
}