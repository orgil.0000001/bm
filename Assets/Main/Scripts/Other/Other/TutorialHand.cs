﻿using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class TutorialHand : Mb {
    public bool isLine = true;
    public Transform handTf;
    public List<Vector3> pnts;
    public int smt = 10;
    public float spc = 10, spd = 500;
    List<Vector3> lis = new List<Vector3>();
    float dis = 0, line = 200;
    Vector3 inf = V3.V(130, 90, 220);
    void Start() {
        if (pnts.IsEmpty())
            pnts = isLine ? List(V3.O, V3.X(line), V3.X(-line)) : List(V3.O, V3.Xy(inf.x, inf.y), V3.X(inf.z), V3.Xy(inf.x, -inf.y), V3.O, V3.Xy(-inf.x, inf.y), V3.X(-inf.z), V3.Xy(-inf.x, -inf.y));
        lis = Crv.CatmullRomSpline(pnts, smt, spc);
    }
    void Update() {
        handTf.position = tf.TfPnt(GetPoint(dis));
        dis += Dt * spd;
    }
    Vector3 GetPoint(float dis) {
        int i = M.FloorI(dis / spc) % lis.Count, i2 = (i + 1) % lis.Count;
        return V3.Lerp(lis[i], lis[i2], dis % spc / spc);
    }
}