using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Orgil;

public class ShadowButton : Shadow, IPointerDownHandler, IPointerUpHandler {
    public float sclTime;
    public Vector2 clickDPos;
    public Color shadowColor;
    public Vector2 shadowDistance;
    RectTransform rt;
    private void OnEnable() {
        rt = gameObject.Rt();
        A.Anim(gameObject, new List<Fo>() { Fo.Fc(0).S(0), Fo.Fc(sclTime * 0.8f).S(1.1f), Fo.Fc(sclTime).S(1) });
    }
    [ContextMenu("Reset")]
    public void Reset() {
        UpdButtonCol();
        effectDistance = new Vector2(0, -20);
        sclTime = 0.25f;
        clickDPos = new Vector2(0, -10);
        shadowColor = new Color(0f, 0f, 0f, 0.2f);
        shadowDistance = new Vector2(10, -10);
    }
    [ContextMenu("Update Button Color")]
    public void UpdButtonCol() {
        effectColor = gameObject.GetCol(C.I).Dv(-0.2f);
    }
    public override void ModifyMesh(VertexHelper vh) {
        if (!IsActive())
            return;
        var verts = new List<UIVertex>();
        vh.GetUIVertexStream(verts);
        var neededCpacity = verts.Count * 5;
        if (verts.Capacity < neededCpacity)
            verts.Capacity = neededCpacity;
        var tmp = verts.Count;
        ApplyShadowZeroAlloc(verts, shadowColor, 0, verts.Count, effectDistance.x + shadowDistance.x, effectDistance.y + shadowDistance.y);
        ApplyShadowZeroAlloc(verts, effectColor, tmp, verts.Count, effectDistance.x, effectDistance.y);
        vh.Clear();
        vh.AddUIVertexTriangleStream(verts);
    }
    public void OnPointerDown(PointerEventData eventData) {
        rt.anchoredPosition += clickDPos;
        effectDistance -= clickDPos;
    }
    public void OnPointerUp(PointerEventData eventData) {
        rt.anchoredPosition -= clickDPos;
        effectDistance += clickDPos;
    }
}