﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;
using Polygon;

public class Bot : Character {
    float rotSpd = 5, minTime = 1.5f, maxTime = 3, curAng = 0, ang = 0, t, dt = 0;
    [HideInInspector]
    public bool isWalk = false, isPunch = false;
    Vector3 pos, dir;
    [HideInInspector]
    public Animator an;
    RaycastHit hit;
    private void Start() {
        an = go.Child(0).An();
        rb.NoG();
        rb.Con(false, true, false, true, true, true);
        Init();
        pos = Tp;
        StaRot();
    }
    void StaRot() {
        List<Vector3> dirs = List(F, B, L, R);
        dirs.Shuffle();
        foreach (var dir in dirs)
            if (BlockCnt(dir) > 0) {
                LookDir(dir);
                break;
            }
    }
    void Update() {
        if (IsPlaying && data.isLive) {
            if (Physics.SphereCast(Tp, 0.3f, dir, out hit, 0.5f, Lm.Bomb)) {
                pos = Ls.I.RoundPos(Tp);
                LookDir(B);
            }
            if (V3.Dis(pos, Tp) > 0.99f) {
                bool isLr = Rnd.P(0.5f);
                int f = BlockCnt(F), l = BlockCnt(L), r = BlockCnt(R);
                pos = Tp = Ls.I.RoundPos(Tp);
                if (isLr) {
                    LookDir(l > 0 && r > 0 ? (Rnd.B ? L : R) : (l > 0 ? L : r > 0 ? R : f > 0 ? F : B));
                } else {
                    LookDir(f > 0 ? F : (l > 0 && r > 0 ? (Rnd.B ? L : R) : (l > 0 ? L : r > 0 ? R : B)));
                }
            }
            if (!isPunch && A.IsEnemyEnter(tf.Tf(), Player.I.Tp, 1, 90))
                StaCor(PunchCor());
            if (!isPunch) {
                rb.V(dir * Ls.I.botWalkSpd);
                if (isWalk) {
                    if (rb.velocity.magnitude < 0.01f) {
                        isWalk = false;
                        an.Anim(Anim.Idle);
                    }
                } else {
                    if (rb.velocity.magnitude > 0.01f) {
                        isWalk = true;
                        an.Anim(Anim.Walk);
                    }
                }
            }
        } else
            rb.V0();
    }
    private void OnCollisionEnter(Collision collision) {
        if (collision.Tag(Tag.Player) && !isPunch) {
            StaCor(PunchCor());
        }
    }
    IEnumerator PunchCor() {
        rb.ConFrzAll();
        tf.LookAt(Player.I.Tp);
        an.Play("Punch");
        isPunch = true;
        yield return Wf.F(15);
        Ac.I.Play("Punch");
        A.Vibrate(Vib.Light);
        Player.I.Death();
        yield return Wf.F(50);
        isPunch = false;
        rb.Con(false, true, false, true, true, true);
        if (IsGameOver) {
            an.Anim(Anim.Victory);
        } else {
            an.Anim(isWalk ? Anim.Walk : Anim.Idle);
            LookDir(dir);
        }
    }
    int BlockCnt(Vector3 dir) {
        int res = 1000;
        if (Physics.SphereCast(Tp, 0.3f, dir, out hit, 1000, Lm.Wall | Lm.Box))
            res = M.RoundI(V3.Dis(Tp, hit.collider.Par().position)) - 1;
        return res;
    }
    public void LookDir(Vector3 dir) {
        tf.LookAt(Tp + dir);
        this.dir = dir;
    }
    public void Death() {
        an.Anim(Anim.Death);
        data.isLive = false;
        Dst(go.Col());
        int i = Bs.I.bots.FindAll(x => !x.data.isLive).Count;
        Cc.I.HudLevelBar(Gc.Level, i.F() / Bs.I.bots.Count);
        if (i == Bs.I.bots.Count) {
            Player.I.an.Anim(Anim.Victory);
            Gc.I.LevelCompleted();
        }
    }
}