﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public enum PickUpTp { None, Speed, Bomb, Power, Health }

public class Level {
    public List<string> data = new List<string>();
    public bool isCrtData = false;
    public int w, h, enemy, box, bomb = 0, power = 0, speed = 0, health = 0;
    public Level(params string[] data) {
        this.data = data.List();
    }
    public Level(int w, int h, int enemy, int box, int bomb = 0, int power = 0, int speed = 0, int health = 0) {
        isCrtData = true;
        this.w = w;
        this.h = h;
        this.enemy = enemy;
        this.box = box;
        this.bomb = bomb;
        this.power = power;
        this.speed = speed;
        this.health = health;
    }
}

[System.Serializable]
public class EnvData {
    public Color wallC, floorC, boxC, playerC, enemyC, bgC;
}

public class Ls : Singleton<Ls> { // Level Spawner
    public List<EnvData> envs = new List<EnvData>() { new EnvData() };
    List<Level> levels = new List<Level>() {
        new Level(11, 9, 1, 6, 2, 2, 0, 0),
        new Level(11, 9, 1, 7, 2, 2, 0, 0),
        new Level(11, 9, 1, 8, 2, 2, 0, 0),
        new Level(11, 11, 2, 9, 3, 2, 1, 0),
        new Level(11, 11, 2, 10, 3, 2, 1, 0),
        new Level(11, 11, 2, 11, 3, 2, 1, 0),
        new Level(13, 11, 2, 12, 3, 2, 1, 1),
        new Level(13, 11, 2, 13, 3, 2, 1, 1),
        new Level(13, 11, 3, 14, 3, 2, 1, 1),
        new Level(13, 13, 3, 15, 3, 3, 2, 2),
        new Level(13, 13, 3, 16, 3, 3, 2, 2),
        new Level(13, 13, 3, 17, 3, 3, 2, 2),
        new Level(15, 13, 3, 18, 3, 3, 3, 3),
        new Level(15, 13, 3, 19, 3, 3, 3, 3),
        new Level(15, 13, 3, 20, 3, 3, 3, 3),
    };
    public GameObject wallPf, floorPf, expPf;
    public Box boxPf;
    public PickUp pickUpPf;
    public Bomb bombPf;
    public float bombExpTm = 5;
    public Color bombExpC = C.r;
    public float playerWalkSpd = 2, botWalkSpd = 2;
    public bool useLvl = false;
    public int lvl = 1;
    public GameObject borderGo, floorGo;
    EnvData env;
    public void Init() {
        if (useLvl)
            Gc.Level = lvl;
        LoadLevel();
        LeaderBoardData.SetDatas();
    }
    void Border(Vector3 sz, Vector2 szI, Vector3 pos) {
        Mesh msh = null;
        Msh.Init(ref msh, borderGo);
        Msh.BoxHole(ref msh, sz, szI, V3.O);
        borderGo.Tp(pos);
        borderGo.RenMatCol(env.wallC);
        floorGo.Tlp(pos.Y(-0.05f));
        floorGo.Tls(sz.Y(0.1f));
        floorGo.RenMatCol(env.floorC);
    }
    public void LoadLevel() {
        env = envs[(Gc.Level - 1) % envs.Count];
        Camera.main.backgroundColor = env.bgC;
        Level lvl = levels[(Gc.Level - 1) % levels.Count];
        if (lvl.isCrtData)
            lvl = new Level(LvlData(lvl.w, lvl.h, lvl.enemy, lvl.box, lvl.bomb, lvl.power, lvl.speed, lvl.health));
        Vector2Int n = V2I.V(lvl.data[0].Length, lvl.data.Count);
        Border(V3.V(n.x, 1, n.y), V2.V(n.x - 2, n.y - 2), V3.V((n.x - 1) / 2f, 0.5f, (1 - n.y) / 2f));
        for (int i = 0; i < n.y; i++) {
            for (int j = 0; j < n.x; j++) {
                char c = lvl.data[i][j].L();
                Vector3 pos = V3.Xz(j, -i);
                if (c == '#' || c == '=') {
                    GameObject wallGo = Ins(wallPf, pos, Q.O, tf);
                    wallGo.Child(0).RenMatCol(env.wallC);
                    if (c == '=')
                        wallGo.Child<MeshRenderer>(0).enabled = false;
                } else if (c == 'o') {
                    Player.I.Tp = pos;
                    Player.I.Child(0, 0).RenMatCol(env.playerC);
                } else if (c == 'x') {
                    Bs.I.Create(pos, V3.O).Child(0, 0).RenMatCol(env.enemyC);
                } else if (c == 's' || c == 'b' || c == 'p' || c == 'h' || c == '0') {
                    Box box = Ins(boxPf, pos, Q.O, tf);
                    box.Child(0).RenMatCol(env.boxC);
                    box.pickUpTp = c == 's' ? PickUpTp.Speed : c == 'b' ? PickUpTp.Bomb : c == 'p' ? PickUpTp.Power : c == 'h' ? PickUpTp.Health : PickUpTp.None;
                }
                //if (c != '#' && c != ' ') {
                //    Ins(floorPf, pos, Q.O, tf).Child(0).RenMatCol(env.floorC);
                //}
            }
        }
    }
    int GetLevelIdx() {
        int res = Gc.Level - 1;
        if (Gc.Level > levels.Count) {
            res = Data.LevelIdx.I();
            if (res < 0 || Gc.IsWin) {
                res = Rnd.Idx(res, levels.Count);
                Data.LevelIdx.Set(res);
            }
        }
        return res;
    }
    public Vector3 RoundPos(Vector3 pos) {
        return pos.RoundI();
    }
    static string[] LvlData(int w, int h, int enemy, int box, int bomb = 0, int power = 0, int speed = 0, int health = 0) {
        Vector2Int sz = V2I.V(w + w.IsEven().I(), h + h.IsEven().I());
        string line1 = '='.N(w), line2 = "=" + '_'.N(w - 2) + "=", line3 = "=" + "_#".N(w / 2 - 1) + "_=";
        List<string> data = new List<string>();
        for (int i = 0; i < h; i++)
            data.Add(i == 0 || i == h - 1 ? line1 : (i - 1).IsEven() ? line2 : line3);
        SetPos(data, sz, V2I.I, 'o');
        for (int i = 0; i < enemy; i++)
            SetPos(data, sz, RndPos(data, sz), 'x');
        for (int i = 0; i < box; i++)
            SetPos(data, sz, RndPos(data, sz), '0');
        for (int i = 0; i < bomb; i++)
            SetPos(data, sz, RndPos(data, sz, '0'), 'b');
        for (int i = 0; i < power; i++)
            SetPos(data, sz, RndPos(data, sz, '0'), 'p');
        for (int i = 0; i < speed; i++)
            SetPos(data, sz, RndPos(data, sz, '0'), 's');
        for (int i = 0; i < health; i++)
            SetPos(data, sz, RndPos(data, sz, '0'), 'h');
        return data.Arr();
    }
    static void SetPos(List<string> data, Vector2Int sz, Vector2Int pos, char c) {
        data[pos.y] = data[pos.y].Sub(0, pos.x) + c + data[pos.y].Sub(pos.x + 1, sz.x - pos.x - 1);
    }
    static Vector2Int RndPos(List<string> data, Vector2Int sz, char r = '_') {
        Vector2Int pos = V2I.O;
        for (int i = 0; i < 1000; i++) {
            pos = V2I.V(Rnd.Rng(1, sz.x - 1), Rnd.Rng(1, sz.y - 1));
            if (data[pos.y][pos.x] == r)
                break;
        }
        return pos;
    }
}
// ░ ▒ ▓ █ ▄ ▀ ■
// ┌ ┬ ┐ ─ ╔ ╦ ╗ ═
// ├ ┼ ┤ │ ╠ ╬ ╣ ║
// └ ┴ ┘   ╚ ╩ ╝
// ⁰¹²³⁴⁵⁶⁷⁸⁹ ⁻⁺⁼⁽⁾ ⁱⁿ superscript
// ₀₁₂₃₄₅₆₇₈₉ ₋₊₌₍₎⨧ ᵣᵤₐᵢⱼₓₑᵥₒₔ ᵪᵧᵦᵨᵩ subscript
// _ ¯ ~ ≡ ‗ ¦ ¨ ¬ · |
// ñ Ñ @ ¿ ? ¡ ! : / \ frequently-used
// á é í ó ú Á É Í Ó Ú vowels acute accent
// ä ë ï ö ü Ä Ë Ï Ö Ü vowels with diaresis
// ½ ¼ ¾ ¹ ³ ² ƒ ± × ÷ mathematical symbols
// $ £ ¥ ¢ ¤ ® © ª º ° commercial / trade symbols
// " ' ( ) [ ] { } « » quotes and parenthesis