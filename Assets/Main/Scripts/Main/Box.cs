using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Box : Mb {
    public PickUpTp pickUpTp;
    public void Death() {
        if (pickUpTp != PickUpTp.None) {
            PickUp pickUp = Ins(Ls.I.pickUpPf, Tp, Tr, Ls.I.tf);
            pickUp.ChildNameShow(pickUpTp.tS());
            pickUp.tp = pickUpTp;
        }
        Dst(go.Child(0).Col());
        go.Child(0).An().enabled = true;
        Dst(go, 0.5f);
    }
}
