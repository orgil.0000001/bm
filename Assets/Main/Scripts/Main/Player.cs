﻿using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;
using Polygon;

public class Player : Character {
    public static Player I {
        get {
            if (_.Null())
                _ = GameObject.FindObjectOfType<Player>();
            return _;
        }
    }
    static Player _;
    float r = 100, dis;
    PolygonChr chr;
    public Text healthTxt, spdTxt, bombTxt, powerTxt;
    bool isSta = false, isClk = false, isWalk = false;
    public int power = 1, health = 1, bombCnt = 1, bombIdx = 0, speed = 1;
    public List<Bomb> bombs = new List<Bomb>();
    [HideInInspector]
    public Animator an;
    private void Awake() {
        _ = this;
        chr = go.Gc<PolygonChr>();
        rb.NoG();
        rb.Con(false, true, false, true, true, true);
        an = go.Child(0).An();
        UpdTxt();
    }
    public void Reset() {
    }
    public void UpdTxt() {
        healthTxt.text = "" + health;
        spdTxt.text = "" + speed;
        bombTxt.text = bombIdx + "/" + bombCnt;
        powerTxt.text = "" + power;
    }
    void Update() {
        if (IsPlaying) {
            if (IsMbD) {
                mp = Mp;
                if (!isSta)
                    isSta = true;
                isClk = true;
            }
            if (IsMb) {
                dis = V3.Dis(Mp, mp);
                if (dis > r)
                    mp = V3.Move(Mp, mp, r);
                Tr = Q.Y(Ang.LookF(mp, Mp));
                rb.V(F * M.C01(dis / r) * (Ls.I.playerWalkSpd + (speed - 1)));
            }
            if (IsMbU) {
                rb.V0();
                if (isSta && bombIdx < bombCnt) {
                    Ins(Ls.I.bombPf, Ls.I.RoundPos(Tp), Q.O, Ls.I.tf);
                    bombIdx++;
                    UpdTxt();
                }
                isClk = false;
            }
            if (!isClk)
                rb.V0();
            if (isWalk) {
                if (rb.velocity.magnitude < 0.01f) {
                    isWalk = false;
                    an.Anim(Anim.Idle);
                }
            } else {
                if (rb.velocity.magnitude > 0.01f) {
                    isWalk = true;
                    an.Anim(Anim.Walk);
                }
            }
        } else
            rb.V0();
    }
    private void OnTriggerEnter(Collider other) {
        if (other.Tag(Tag.PickUp)) {
            PickUpTp tp = other.Gc<PickUp>().tp;
            if (tp == PickUpTp.Speed) {
                speed++;
            } else if (tp == PickUpTp.Bomb) {
                bombCnt++;
            } else if (tp == PickUpTp.Power) {
                power++;
            } else if (tp == PickUpTp.Health) {
                health++;
            }
            UpdTxt();
            Dst(other.gameObject);
        }
    }
    public void Death() {
        health--;
        if (health <= 0) {
            an.Anim(Anim.Death);
            Ivk(nameof(DeathSound), 1.5f);
            foreach (var bot in Bs.I.bots)
                if (bot.data.isLive && !bot.isPunch)
                    bot.an.Anim(Anim.Victory);
            Gc.I.GameOver();
        }
    }
    void DeathSound() {
        Ac.I.Play("Death");
        A.Vibrate(Vib.Light);
    }
}