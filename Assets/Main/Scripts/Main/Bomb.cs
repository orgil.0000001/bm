using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Bomb : Mb {
    float tm;
    Material mat, mat2;
    Color staC;
    private void Start() {
        tm = Ls.I.bombExpTm;
        mat = go.Child(0, 0).RenMat();
        mat2 = go.Child(0, 1).RenMat();
        staC = mat.color;
    }
    private void Update() {
        if (IsPlaying) {
            tm -= Dt;
            Color c = C.Lerp(C.r, staC, tm / Ls.I.bombExpTm);
            mat2.color = mat.color = c;
            if (tm < 0) {
                DstBox(F);
                DstBox(B);
                DstBox(L);
                DstBox(R);
                CrtExp(Tp);
                Ac.I.Play("Exp");
                A.Vibrate(Vib.Medium);
                Dst(go);
                Player.I.bombIdx--;
                Player.I.UpdTxt();
            }
        }
    }
    void DstBox(Vector3 dir) {
        float dis = Player.I.power + 0.5f;
        RaycastHit hit;
        if (Physics.SphereCast(Tp, 0.3f, dir, out hit, Player.I.power, Lm.Wall | Lm.Box)) {
            dis = V3.Dis(Tp, hit.collider.Par().position);
            if (hit.collider.Tag(Tag.Box))
                hit.collider.Par<Box>().Death();
        }
        for (int i = 1; i < dis; i++)
            CrtExp(Tp + dir * i);
        RaycastHit[] hits = Physics.SphereCastAll(Tp, 0.49f, dir, Player.I.power, Lm.Player | Lm.Bot);
        foreach (var hit2 in hits) {
            if (dis > V3.Dis(Tp, Ls.I.RoundPos(hit2.collider.Tp()))) {
                if (hit2.collider.Tag(Tag.Player))
                    Player.I.Death();
                else if (hit2.collider.Tag(Tag.Bot))
                    hit2.collider.Gc<Bot>().Death();
            }
        }
    }
    void CrtExp(Vector3 pos) {
        Dst(Ins(Ls.I.expPf, pos, Q.O, Ls.I.tf), 3);
    }
}
