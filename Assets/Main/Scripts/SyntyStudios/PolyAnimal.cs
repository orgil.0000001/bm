using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;
using System;

namespace Polygon {
    public enum LowPolyAnimal {
        Bear_Brown, Bear_Polar,
        Cat_Black, Cat_Orange, Cat_White,
        Cow_Black, Cow_Brown, Cow_Spotty, Cow_White,
        Deer_Brown, Deer_Grey,
        Dog_Chihauhau_Black, Dog_Chihauhau, Dog_Golden_Retriever, Dog_Great_Dane_Black, Dog_Great_Dane_Brown,
        Elephant, Fish, Giraffe, Penguin, Seagull,
        Horse_Black, Horse_Brown, Horse_Light, Horse_White,
        Rabbit_Brown, Rabbit_White,
        Shark_Black, Shark_White,
        Snake_Black, Snake_Red,
        Spider_Black, Spider_Green, Spider_Red,
        Wolf_Black, Wolf_White,
    }
    public enum LowPolyAnimalAnim {
        Bear_Idle, Bear_Walk, Bear_Stand_Up, Bear_Death,
        Cat_Idle, Cat_Walk, Cat_StandUp, Cat_Sit, Cat_Sleep, Cat_Run, Cat_Attack, Cat_Death,
        Cow_Idle, Cow_Walk, Cow_Run, Cow_Eat, Cow_Death,
        Deer_Idle, Deer_Walk, Deer_Run, Deer_Eat, Deer_Death,
        Dog_Chihuahua_Idle, Dog_Chihuahua_Walk, Dog_Chihuahua_Run, Dog_Chihuahua_Sit, Dog_Chihuahua_Bark,
        Dog_GoldenRetriever_Idle, Dog_GoldenRetriever_Walk, Dog_GoldenRetriever_Run, Dog_GoldenRetriever_Stand, Dog_GoldenRetriever_Sit, Dog_GoldenRetriever_Bark,
        Dog_GreatDane_Idle, Dog_GreatDane_Walk, Dog_GreatDane_Run, Dog_GreatDane_Stand, Dog_GreatDane_Sit, Dog_GreatDane_Bark,
        Elephant_Idle, Elephant_Walk, Elephant_Water, Elephant_Death,
        Fish_Swim,
        Giraffe_Idle, Giraffe_Walk, Giraffe_Eat, Giraffe_Death,
        Horse_Idle, Horse_Walk, Horse_Run, Horse_Eat, Horse_Death,
        Penguin_Idle, Penguin_Walk, Penguin_Shake, Penguin_Death,
        Rabbit_Idle, Rabbit_Run, Rabbit_Jump_Up, Rabbit_Jump_Walk, Rabbit_LookOut, Rabbit_Death_0, Rabbit_Death_1,
        Seagull_Fly, Seagull_Sit,
        Shark_Swim, Shark_Attack, Shark_Death,
        Snake_Idle, Snake_Slither, Snake_Attack, Snake_Death,
        Spider_Idle, Spider_Walk, Spider_Scare, Spider_Attack, Spider_Death_1, Spider_Death_2, Spider_Death_3,
        Wolf_Idle, Wolf_Walk, Wolf_Run, Wolf_Howl, Wolf_Attack_Idle, Wolf_Attack, Wolf_Death,
    }
    public enum PolyArtAnimal {
        Bear_Black, Bear_Cub, Bear_Grey, Bear_Grizly, Bear_Panda, Bear_Polar,
        Boar_Baby, Boar_Black, Boar_Gold, Boar_Grey, Boar_Red, Boar,
        Cougar_Cub, Cougar_Leopard, Cougar_Panther, Cougar_Puma, Cougar_Skinny, Cougar,
        Deer_Female, Deer, Deer_Elk_Albine, Deer_Elk, Deer_Fawn,
        Fox_Black, Fox_Corgi, Fox_Cub, Fox_Fennec, Fox_Mane_Wolf, Fox_Snow, Fox,
        Moose_Albine, Moose_Baby, Moose_Black, Moose_Female, Moose,
        Rabbit_Baby, Rabbit_Black, Rabbit_Brown_White, Rabbit_Brown, Rabbit_Common, Rabbit_White_Spots, Rabbit_White,
        Raccoon_Albine, Raccoon_Black, Raccoon_Brown, Raccoon_Cub, Raccoon_Red_Panda, Raccoon,
        Tiger_Albine, Tiger_Bengala, Tiger_Fire, Tiger_Magic, Tiger_White,
        Wolf_Black, Wolf_Brown, Wolf_Cub, Wolf_Grey, Wolf_White,
    }
    public enum PolyArtAtc {
        Bear_Black, Bear_Brown, Bear_Grey, Bear_Grizly, Bear_Panda, Bear_Polar,
        Boar_Black, Boar_Gold, Boar_Grey, Boar_Red, Boar,
        Cougar_Leopard, Cougar_Panther, Cougar_Puma, Cougar,
        Deer_2, Deer_Albine, Deer_Antlers, Deer_Elk, Deer_Fawn, Deer,
        Fox_Black, Fox_Fennec, Fox_Mane_Wolf, Fox_Orange, Fox_Snow,
        Moose_2, Moose_Albino, Moose_Antlers, Moose_Black, Moose,
        Rabbit_Black, Rabbit_Brown_White, Rabbit_Brown, Rabbit_Grey_White, Rabbit_White_Spots, Rabbit_White, Rabbit,
        Raccoon_Albine, Raccoon_Black, Raccoon_Brown, Raccoon_Gray, Raccoon_Red_Panda, Raccoon,
        Tiger_Albine, Tiger_Bengala, Tiger_Fire, Tiger_Magic, Tiger_White,
        Wolf_Black, Wolf_Brown, Wolf_Grey, Wolf_White,
        Boar_Tusk_Blood, Boar_Tusk_Dirty, Boar_Tusk_White,
        Spots_Cougar, Spots_Deer,
        Fangs_Null, Fangs__001, Fangs__002, Fangs__003, Fangs__004, Fangs__005, Fangs__006,
        Antlers_Null, Antlers__01, Antlers__02, Antlers__03, Antlers__04, Antlers__05,
        Magic_Null, Magic_Black, Magic_Blue, Magic_Green, Magic_Red, Magic_White, Magic_Yellow,
    }
    public enum PolyArtAnimalAnim {
        // Bear
        Bear_Idle_1, Bear_Idle_Smell, Bear_Idle_Look_Left, Bear_Dig, Bear_Idle_To_Stand, Bear_Stand_Idle_1, Bear_Stand_Idle_Scratch_Neck, Bear_Stand_Roar, Bear_Stand_To_Idle,
        Bear_Walk, Bear_Walk_R, Bear_Walk_L, Bear_Walk_BL, Bear_Walk_BR, Bear_Walk_B,
        Bear_Trot, Bear_Trot_R, Bear_Trot_L, Bear_Run, Bear_Run_R, Bear_Run_L,
        Bear_Idle_To_Seat, Bear_Seat_Idle_1, Bear_Seat_Hi, Bear_Seat_Look_Around, Bear_Seat_To_Sleep, Bear_Sleep_Idle, Bear_Sleep_To_Seat, Bear_Seat_To_Idle,
        Bear_Swim, Bear_Swim_R, Bear_Swim_L, Bear_Swim_Idle, Bear_Swim_Turn_R, Bear_Swim_Turn_L, Bear_Swim_B, Bear_Swim_BL, Bear_Swim_BR, Bear_Swim_Enter,
        Bear_Jump_Trot_Baked, Bear_Jump_Trot, Bear_Jump_Run_Baked, Bear_Jump_Run, Bear_Land_F, Bear_Land_Run,
        Bear_Fall_From_Edge, Bear_Fall_High, Bear_Fall_Low, Bear_Recover, Bear_Land,
        Bear_Turn_L_180, Bear_Turn_L, Bear_Turn_R_180, Bear_Turn_R,
        Bear_Drink, Bear_Eat, Bear_Stunned, Bear_Roar, Bear_Stand_Push, Bear_Stand_Push_Loop, Bear_Eat_Loop, Bear_Drink_Loop,
        Bear_Attack_Bite_L, Bear_Attack_Bite_R, Bear_Attack_Paws, Bear_Attack_Paw_R, Bear_Attack_Paw_L,
        Bear_Damaged_FR, Bear_Damaged_L, Bear_Damaged_BL, Bear_Damaged_FL, Bear_Damaged_R, Bear_Damaged_BR, Bear_Damaged_R_Mask, Bear_Damaged_L_Mask,
        Bear_Death_1, Bear_Death_2, Bear_Death_1_Opp, Bear_Death_2_Opp,
        // Boar
        Boar_Idle, Boar_Idle_Small_Shake, Boar_Idle_Big_Shake, Boar_Idle_Look,
        Boar_Walk, Boar_Walk_R, Boar_Walk_L, Boar_Walk_BL, Boar_Walk_BR, Boar_Walk_B,
        Boar_Trot, Boar_Trot_R, Boar_Trot_L, Boar_Run, Boar_Run_R, Boar_Run_L, Boar_Attack_Run, Boar_Attack_Run_Mask,
        Boar_Stand_To_Seat, Boar_Seat_Idle, Boar_Seat_To_Sleep, Boar_Sleep_Idle, Boar_Sleep_To_Stand,
        Boar_Swim, Boar_Swim_Idle, Boar_Swim_Turn_L, Boar_Swim_Turn_R, Boar_Swim_R, Boar_Swim_L, Boar_Swim_B, Boar_Swim_BL, Boar_Swim_BR,
        Boar_Jump_Trot, Boar_Jump_Sprint, Boar_Jump_Run, Boar_Jump_Trot_Baked, Boar_Jump_Sprint_Baked, Boar_Jump_Run_Baked, Boar_Trot_Land, Boar_Run_Land, Boar_Sprint_Land,
        Boar_Edge_Jump, Boar_Fall_High, Boar_Fall_Recover, Boar_Fall_Water, Boar_Fall_Low,
        Boar_Turn_R, Boar_Turn_L, Boar_Turn_R_180, Boar_Turn_L_180,
        Boar_Drink, Boar_Eat,
        Boar_Attack_Fangs_1, Boar_Attack_Fangs_2, Boar_Attack_Fangs_Front, Boar_Attack_Bite,
        Boar_Get_Hit_R, Boar_Get_Hit_FL, Boar_Get_Hit_BR, Boar_Get_Hit_L, Boar_Get_Hit_FR, Boar_Get_Hit_BL, Boar_Get_Hit_R_Mask, Boar_Get_Hit_L_Mask,
        Boar_Death_Side, Boar_Death_2, Boar_Death_Side_Opp, Boar_Death_2_Opp,
        // Cougar
        Cougar_Idle_1, Cougar_Idle_2, Cougar_Idle_3, Cougar_Idle_4,
        Cougar_Walk, Cougar_Walk_R, Cougar_Walk_L, Cougar_Walk_BL, Cougar_Walk_BR, Cougar_Walk_B,
        Cougar_Trot, Cougar_Trot_R, Cougar_Trot_L, Cougar_Run, Cougar_Run_R, Cougar_Run_L,
        Cougar_Stand_To_Seat, Cougar_Seat_Idle, Cougar_Seat_To_Lie, Cougar_Laying_Idle_1, Cougar_Laying_Idle_2, Cougar_Laying_To_Sleep, Cougar_Sleep, Cougar_Sleep_To_Lie, Cougar_Lie_To_Seat, Cougar_Seat_To_Stand,
        Cougar_Swim, Cougar_Swim_R, Cougar_Swim_L, Cougar_Swim_Idle, Cougar_Swim_Turn_R, Cougar_Swim_Turn_L, Cougar_Swim_B, Cougar_Swim_BL, Cougar_Swim_BR, Cougar_Swim_Enter,
        Cougar_Jump_In_Place_Bake, Cougar_Jump_F, Cougar_Jump_Run, Cougar_Jump_F_Bake, Cougar_Jump_Run_Bake, Cougar_Jump_In_Place, Cougar_Land_In_Place, Cougar_Land_F, Cougar_Land_Run,
        Cougar_Fall_Edge, Cougar_Fall_High, Cougar_Fall_Low, Cougar_Fall_Recover,
        Cougar_Turn_L_180, Cougar_Turn_R_180, Cougar_Turn_L_90, Cougar_Turn_R_90,
        Cougar_Dig, Cougar_Drink, Cougar_Eat, Cougar_Stun, Cougar_Crawl_Under,
        Cougar_Attack_Bite_L, Cougar_Attack_Bite_R, Cougar_Attack_Claws, Cougar_Attack_Jump, Cougar_Attack_Paw_L, Cougar_Attack_Paw_R,
        Cougar_Damaged_FR, Cougar_Damaged_FL, Cougar_Damaged_L, Cougar_Damaged_R, Cougar_Damaged_BL, Cougar_Damaged_BR, Cougar_Roar, Cougar_Damaged_FR_Mask, Cougar_Damaged_FL_Mask, Cougar_Damaged_L_Mask, Cougar_Damaged_R_Mask, Cougar_Damaged_BL_Mask, Cougar_Damaged_BR_Mask,
        Cougar_Death_1, Cougar_Death_2,
        // Deer
        Deer_Idle_1, Deer_Idle_Head_Shake, Deer_Idle_Look, Deer_Idle_Scratch,
        Deer_Walk, Deer_Walk_R, Deer_Walk_L, Deer_Walk_BL, Deer_Walk_BR, Deer_Walk_B,
        Deer_Trot, Deer_Trot_R, Deer_Trot_L, Deer_Run, Deer_Run_R, Deer_Run_L, Deer_Attack_Run,
        Deer_Stand_To_Seat, Deer_Seat_Idle, Deer_Seat_To_Sleep, Deer_Sleep_Idle, Deer_Sleep_To_Stand,
        Deer_Swim, Deer_Swim_Idle, Deer_Swim_Turn_L, Deer_Swim_Turn_R, Deer_Swim_R, Deer_Swim_L, Deer_Swim_B, Deer_Swim_BL, Deer_Swim_BR,
        Deer_Jump_Trot, Deer_Jump_Run, Deer_Jump_Trot_Baked, Deer_Jump_Run_Baked, Deer_Jump_Trot_Land, Deer_Jump_Run_Land, Deer_Land_Trot, Deer_Land_Run,
        Deer_Edge_Jump, Deer_Fall_High, Deer_Fall_Recover, Deer_Fall_Water, Deer_Fall_Low,
        Deer_Turn_R, Deer_Turn_L, Deer_Turn_L_180, Deer_Turn_R_180,
        Deer_Drink, Deer_Eat,
        Deer_Attack_B_Legs, Deer_Attack_F_Legs, Deer_Attack_B_Leg, Deer_Attack_Horns_1, Deer_Attack_Horns_2,
        Deer_Get_Hit_R, Deer_Get_Hit_FR, Deer_Get_Hit_BR, Deer_Get_Hit_L, Deer_Get_Hit_FL, Deer_Get_Hit_BL, Deer_Get_Hit_R_Mask, Deer_Get_Hit_L_Mask,
        Deer_Death_Side, Deer_Death_2, Deer_Death_Run, Deer_Death_Side_Opp, Deer_Death_2_Opp, Deer_Death_Run_Opp,
        // Fox
        Fox_Idle_1, Fox_Idle_2, Fox_Idle_3, Fox_Idle_4,
        Fox_Walk, Fox_Walk_L, Fox_Walk_R, Fox_Walk_BL, Fox_Walk_BR, Fox_Walk_B,
        Fox_Trot, Fox_Trot_R, Fox_Trot_L, Fox_Run, Fox_Run_R, Fox_Run_L,
        Fox_Stand_To_Seat, Fox_Seat_Idle, Fox_Seat_To_Lie, Fox_Lie_Idle_1, Fox_Lie_Idle_2, Fox_Lie_To_Sleep, Fox_Sleep, Fox_Sleep_To_Seat, Fox_Seat_To_Stand, Fox_Lie_To_Seat,
        Fox_Swim, Fox_Swim_R, Fox_Swim_L, Fox_Swim_Idle, Fox_Swim_Turn_R, Fox_Swim_Turn_L, Fox_Swim_B, Fox_Swim_BL, Fox_Swim_BR, Fox_Swim_Enter,
        Fox_Jump_F, Fox_Jump_Run, Fox_Jump_InPlace, Fox_Jump_F_Baked, Fox_Jump_Run_Baked, Fox_Jump_In_Place_Baked, Fox_Land_In_Place, Fox_Land_F, Fox_Land_Run,
        Fox_Fall_Edge, Fox_Fall_High, Fox_Fall_Low, Fox_Fall_Recover, Fox_Fall_Small_Recover,
        Fox_Turn_L_180, Fox_Turn_R_180, Fox_Turn_L_90, Fox_Turn_R_90,
        Fox_Drink, Fox_Eat, Fox_Stun, Fox_Dig, Fox_Crawl,
        Fox_Attack_Bite_L, Fox_Attack_Bite_R, Fox_Attack_Claws, Fox_Attack_Jump,
        Fox_Damaged_FR, Fox_Damaged_FL, Fox_Damaged_L, Fox_Damaged_R, Fox_Damaged_BL, Fox_Damaged_BR, Fox_Damaged_R_Mask, Fox_Damaged_L_Mask, Fox_Bark,
        Fox_Death_1, Fox_Death_2,
        // Moose
        Moose_Idle_1, Moose_Idle_Head_Shake, Moose_Idle_Look, Moose_Idle_Look_2,
        Moose_Walk, Moose_Walk_R, Moose_Walk_L, Moose_Walk_BL, Moose_Walk_BR, Moose_Walk_B,
        Moose_Trot, Moose_Trot_R, Moose_Trot_L, Moose_Run, Moose_Run_R, Moose_Run_L, Moose_Attack_Run, Moose_Attack_Run_Mask,
        Moose_Stand_To_Seat, Moose_Seat_Idle, Moose_Seat_To_Sleep, Moose_Sleep_Idle, Moose_Sleep_To_Stand,
        Moose_Swim_F, Moose_Swim_Idle, Moose_Swim_Turn_L, Moose_Swim_Turn_R, Moose_Swim_R, Moose_Swim_L, Moose_Swim_B, Moose_Swim_BL, Moose_Swim_BR,
        Moose_Jump_Trot, Moose_Jump_Run, Moose_Jump_Trot_Baked, Moose_Jump_Run_Baked, Moose_Land_Trot, Moose_Land_Run,
        Moose_Edge_Jump, Moose_Fall_High, Moose_Fall_Recover, Moose_Fall_Water, Moose_Fall_Low,
        Moose_Turn_R, Moose_Turn_L, Moose_Turn_L_180, Moose_Turn_R_180,
        Moose_Drink, Moose_Eat,
        Moose_Attack_B_Legs, Moose_Attack_F_Legs, Moose_Attack_B_Leg, Moose_Attack_Horns_1, Moose_Attack_Horns_2,
        Moose_Get_Hit_R, Moose_Get_Hit_FR, Moose_Get_Hit_BR, Moose_Get_Hit_L, Moose_Get_Hit_FL, Moose_Get_Hit_BL, Moose_Get_Hit_R_Mask, Moose_Get_Hit_L_Mask,
        Moose_Death_Side, Moose_Death_2, Moose_Death_Run, Moose_Death_Side_Opp, Moose_Death_2_Opp, Moose_Death_Run_Opp,
        // Rabbit
        Rabbit_Idle_1, Rabbit_Idle_Groom, Rabbit_Idle_Stand, Rabbit_Idle_Look, Rabbit_Groom_Loop_1, Rabbit_Groom_Loop_2, Rabbit_Groom_Loop_3, Rabbit_Groom_Loop_4,
        Rabbit_Walk, Rabbit_Walk_R, Rabbit_Walk_L, Rabbit_Walk_BL, Rabbit_Walk_BR, Rabbit_Walk_B,
        Rabbit_Trot, Rabbit_Trot_R, Rabbit_Trot_L, Rabbit_Run, Rabbit_Run_R, Rabbit_Run_L,
        Rabbit_Idle_To_Lie, Rabbit_Lie_Idle_1, Rabbit_Lie_Idle_2, Rabbit_Lie_To_Sleep, Rabbit_Sleep, Rabbit_Sleep_To_Lie, Rabbit_Lie_To_Idle,
        Rabbit_Swim_F, Rabbit_Swim_R, Rabbit_Swim_L, Rabbit_Swim_Idle, Rabbit_Swim_Turn_R, Rabbit_Swim_Turn_L, Rabbit_Swim_B, Rabbit_Swim_BL, Rabbit_Swim_BR, Rabbit_Enter_Water,
        Rabbit_Jump_In_Place, Rabbit_Jump_F, Rabbit_Jump_Run, Rabbit_Jump_In_Place_Baked, Rabbit_Jump_F_Baked, Rabbit_Jump_Run_Baked, Rabbit_Land_In_Place, Rabbit_Land_F, Rabbit_Land_Run,
        Rabbit_Fall_From_Edge, Rabbit_Fall_High, Rabbit_Fall_Low, Rabbit_Land,
        Rabbit_Turn_R, Rabbit_Turn_L, Rabbit_Turn_R_180, Rabbit_Turn_L_180,
        Rabbit_Drink, Rabbit_Eat, Rabbit_Dig, Rabbit_Hole_Enter, Rabbit_Hole_Stay, Rabbit_Hole_Exit, Rabbit_Hang_Death,
        Rabbit_Attack_Paw, Rabbit_Attack_Bite, Rabbit_Attack_Foot, Rabbit_Attack_Feet_360,
        Rabbit_Damage_FL, Rabbit_Damage_L, Rabbit_Damage_BL, Rabbit_Damage_FR, Rabbit_Damage_R, Rabbit_Damage_BR, Rabbit_Stun, Rabbit_Damage_L_Mask, Rabbit_Damage_R_Mask,
        Rabbit_Death_1, Rabbit_Death_2, Rabbit_Death_3, Rabbit_Death_1_Opp, Rabbit_Death_2_Opp, Rabbit_Death_3_Opp,
        // Raccoon
        Raccoon_Idle_1, Raccoon_Idle_Scratch, Raccoon_Idle_Look_R, Raccoon_Idle_Smell, Raccoon_Idle_Look_L, Raccoon_Idle_Yaw, Raccoon_Idle_To_Stand, Raccoon_Stand_Idle, Raccoon_Stand_Look_R, Raccoon_Stand_To_Idle, Raccoon_Sneak_Idle, Raccoon_Sneak_Look_R, Raccoon_Stand_Look_L, Raccoon_Sneak_Look_L, Raccoon_Wounded_Idle,
        Raccoon_Walk, Raccoon_Walk_L, Raccoon_Walk_R, Raccoon_Walk_2, Raccoon_Walk_L_2, Raccoon_Walk_R_2, Raccoon_Walk_BR, Raccoon_Walk_BL, Raccoon_Walk_B,
        Raccoon_Walk_2_Feet, Raccoon_Walk_2_Feet_L, Raccoon_Walk_2_Feet_R, Raccoon_Walk_Wounded, Raccoon_Walk_Wounded_L, Raccoon_Walk_Wounded_R,
        Raccoon_Trot, Raccoon_Trot_L, Raccoon_Trot_R, Raccoon_Run, Raccoon_Run_L, Raccoon_Run_R,
        Raccoon_Sneak, Raccoon_Sneak_L, Raccoon_Sneak_R, Raccoon_Dodge_L, Raccoon_Dodge_R, Raccoon_Dodge_B, Raccoon_Dodge_BR, Raccoon_Dodge_BL, Raccoon_Dodge, Raccoon_Dodge_FR, Raccoon_Dodge_FL,
        Raccoon_Idle_To_Seat, Raccoon_Seat, Raccoon_Seat_To_Lie, Raccoon_Lie_1, Raccoon_Lie_2, Raccoon_Lie_To_Sleep, Raccoon_Sleep, Raccoon_Sleep_To_Lie, Raccoon_Lie_To_Seat, Raccoon_Seat_to_Idle,
        Raccoon_Swim, Raccoon_Swim_R, Raccoon_Swim_L, Raccoon_Swim_Idle, Raccoon_Swim_Turn_R, Raccoon_Swim_Turn_L, Raccoon_Swim_B, Raccoon_Swim_BL, Raccoon_Swim_BR, Raccoon_Swim_Enter,
        Raccoon_Jump_In_Place, Raccoon_Jump_In_Place_Baked, Raccoon_Jump_F, Raccoon_Jump_F_Baked, Raccoon_Jump_Run, Raccoon_Jump_Run_Baked, Raccoon_Land_In_Place, Raccoon_Land_F, Raccoon_Land_Run,
        Raccoon_Fall_Edge, Raccoon_Fall_High, Raccoon_Fall_Low, Raccoon_Recover,
        Raccoon_Climb_Start, Raccoon_Climb_Land, Raccoon_Climb_U, Raccoon_Climb_R, Raccoon_Climb_UR, Raccoon_Climb_DR, Raccoon_Climb_L, Raccoon_Climb_UL, Raccoon_Climb_DL, Raccoon_Climb_Idle, Raccoon_Climb_Jump_D, Raccoon_Climb_Cliff_Baked, Raccoon_Climb_Cliff, Raccoon_Climb_D,
        Raccoon_Roll_Full, Raccoon_Roll_Start, Raccoon_Roll_Loop, Raccoon_Roll_End, Raccoon_Roll_Fast_Loop_1, Raccoon_Roll_Fast_Loop_2, Raccoon_Roll,
        Raccoon_Turn_L_180, Raccoon_Turn_L, Raccoon_Turn_R_180, Raccoon_Turn_R, Raccoon_Turn_L_180_Sneak, Raccoon_Turn_L_Sneak, Raccoon_Turn_R_180_Sneak, Raccoon_Turn_R_Sneak, Raccoon_Turn_L_180_Stand, Raccoon_Turn_L_Stand, Raccoon_Turn_R_180_Stand, Raccoon_Turn_R_Stand,
        Raccoon_Action_Dig, Raccoon_Action_Eat, Raccoon_Action_Drink, Raccoon_Stun, Raccoon_Action_Crawl,
        Raccoon_Attack_Bite_L, Raccoon_Attack_Bite_R, Raccoon_Attack_Paws, Raccoon_Attack_Paw_R, Raccoon_Attack_Paw_L, Raccoon_Idle_Attack_Mode,
        Raccoon_Get_Hit_FL, Raccoon_Get_Hit_FR, Raccoon_Get_Hit_R, Raccoon_Get_Hit_BR, Raccoon_Get_Hit_L_Mask, Raccoon_Get_Hit_R_Mask, Raccoon_Get_Hit_FR_2, Raccoon_Get_Hit_FL_2, Raccoon_Get_Hit_L, Raccoon_Get_Hit_BL,
        Raccoon_Death_1, Raccoon_Death_2, Raccoon_Death_1_Opp, Raccoon_Death_2_Opp,
        // Tiger
        Tiger_Idle_1, Tiger_Idle_2, Tiger_Idle_3,
        Tiger_Walk, Tiger_Walk_L, Tiger_Walk_R, Tiger_Walk_BL, Tiger_Walk_BR, Tiger_Walk_B,
        Tiger_Trot, Tiger_Trot_R, Tiger_Trot_L, Tiger_Run, Tiger_Run_R, Tiger_Run_L,
        Tiger_Stand_To_Seat, Tiger_Seat_Idle, Tiger_Seat_To_Lay, Tiger_Lay_Idle_1, Tiger_Lay_Idle_2, Tiger_Lay_To_Sleep, Tiger_Sleep, Tiger_Sleep_To_Seat, Tiger_Seat_To_Stand, Tiger_Lie_To_Seat,
        Tiger_Swim, Tiger_Swim_R, Tiger_Swim_L, Tiger_Swim_Idle, Tiger_Swim_Turn_R, Tiger_Swim_Turn_L, Tiger_Swim_B, Tiger_Swim_BL, Tiger_Swim_BR, Tiger_Swim_Enter,
        Tiger_Jump_In_Place, Tiger_Jump_F, Tiger_Jump_Run, Tiger_Jump_F_Baked, Tiger_Jump_Run_Baked, Tiger_Jump_In_Place_Baked, Tiger_Land_In_Place, Tiger_Land_F, Tiger_Land_Run,
        Tiger_Fall_Edge, Tiger_Fall_High, Tiger_Fall_Low, Tiger_Fall_Recover,
        Tiger_Turn_R_180, Tiger_Turn_L_180, Tiger_Turn_L_90, Tiger_Turn_R_90,
        Tiger_Drink, Tiger_Eat, Tiger_Stun, Tiger_Crawl,
        Tiger_Attack_Bite_L, Tiger_Attack_Bite_R, Tiger_Attack_Claws, Tiger_Attack_Jump,
        Tiger_Damaged_FR, Tiger_Damaged_FL, Tiger_Damaged_L, Tiger_Damaged_R, Tiger_Damaged_BL, Tiger_Damaged_BR, Tiger_Roar, Tiger_Damaged_R_Mask, Tiger_Damaged_L_Mask,
        Tiger_Death_1, Tiger_Death_2,
        // Wolf
        Wolf_Idle_1, Wolf_Idle_2, Wolf_Idle_3, Wolf_Idle_4, Wolf_Combat_Idle,
        Wolf_Walk, Wolf_Walk_L, Wolf_Walk_R, Wolf_Walk_BL, Wolf_Walk_BR, Wolf_Walk_B,
        Wolf_Trot, Wolf_Trot_R, Wolf_Trot_L, Wolf_Run, Wolf_Run_R, Wolf_Run_L,
        Wolf_Stand_To_Seat, Wolf_Seat_Idle, Wolf_Seat_To_Lay, Wolf_Lay_Idle_1, Wolf_Lay_Idle_2, Wolf_Lay_To_Sleep, Wolf_Sleep, Wolf_Sleep_To_Seat, Wolf_Seat_To_Stand, Wolf_Lie_To_Seat,
        Wolf_Swim, Wolf_Swim_R, Wolf_Swim_L, Wolf_Swim_Idle, Wolf_Swim_Turn_R, Wolf_Swim_Turn_L, Wolf_Swim_B, Wolf_Swim_BL, Wolf_Swim_BR, Wolf_Swim_Enter,
        Wolf_Jump_In_Place, Wolf_Jump_F, Wolf_Jump_Run, Wolf_Jump_In_Place_S, Wolf_Jump_In_Place_E, Wolf_Jump_In_Place_All, Wolf_Jump_F_Baked, Wolf_Jump_Run_Baked, Wolf_Jump_In_Place_Baked, Wolf_Land_In_Place, Wolf_Land_F, Wolf_Land_Run, Wolf_Jump_Run_Tag,
        Wolf_Fall_Edge, Wolf_Fall_High, Wolf_Fall_Low, Wolf_Fall_Recover,
        Wolf_Turn_L_180, Wolf_Turn_R_180, Wolf_Turn_L_90, Wolf_Turn_R_90, Wolf_Turn_L_180_Combat, Wolf_Turn_R_180_Combat, Wolf_Turn_L_90_Combat, Wolf_Turn_R_90_Combat,
        Wolf_Drink, Wolf_Eat, Wolf_Stun, Wolf_Hawl, Wolf_Dig, Wolf_Crawl,
        Wolf_Attack_Bite_L, Wolf_Attack_Bite_R, Wolf_Attack_Claws, Wolf_Attack_Jump,
        Wolf_Damaged_FR, Wolf_Damaged_FL, Wolf_Damaged_L, Wolf_Damaged_R, Wolf_Damaged_BL, Wolf_Damaged_BR, Wolf_Damaged_FR_Mask, Wolf_Damaged_FL_Mask, Wolf_Damaged_L_Mask, Wolf_Damaged_R_Mask,
        Wolf_Death_1, Wolf_Death_2,
    }
    public partial class Poly : Mb {
        public static GameObject CrtAnimal(Transform parTf, LowPolyAnimal animal) {
            GameObject pfGo = A.LoadGo("LowPolyAnimal/Prefabs/" + animal);
            return Ins(pfGo, parTf.TfPnt(pfGo.Tlp()), parTf.rotation * pfGo.Tlr(), parTf);
        }
        public static GameObject CrtAnimal(Transform parTf, PolyArtAnimal animal, params PolyArtAtc[] atcs) {
            string type = animal.tS().Split0("_")[0];
            GameObject pfGo = A.LoadGo("PolyArtAnimal/" + type + "/Prefabs/" + animal);
            GameObject go = Ins(pfGo, parTf.TfPnt(pfGo.Tlp()), parTf.rotation * pfGo.Tlr(), parTf);
            GameObject fangGo = null;
            PolyArtAtc boarTusk = PolyArtAtc.Boar;
            for (int i = 0; i < atcs.Length; i++) {
                string name = ReplaceName(atcs[i].tS());
                if (name.IsS("Magic")) {
                    GameObject magicGo = go.ChildGo(1, 1);
                    if (magicGo) {
                        if (atcs[i] == PolyArtAtc.Magic_Null) {
                            magicGo.Hide();
                        } else {
                            magicGo.Show();
                            magicGo.RenMat(A.Load<Material>("PolyArtAnimal/Materials/" + name));
                        }
                    }
                } else if (name.IsS("Antlers") || name.IsS("Fangs")) {
                    bool isAntlers = name.IsS("Antlers");
                    GameObject parGo = isAntlers ? go.ChildNameGo("CG", "Pelvis", "Spine", "Spine1", "Spine2", "Neck", "Neck1", "Head") : go.ChildNameGo("CG", "Pelvis", "Spine", "Spine1", "Spine2", "Neck", "Head", "Husks");
                    for (int j = 0; j < parGo.Tcc(); j++) {
                        GameObject childGo = parGo.ChildGo(j);
                        if (childGo.name.IsS(isAntlers ? "Antlers" : "Fangs")) {
                            childGo.Active(childGo.name == name);
                            if (childGo.name == name)
                                fangGo = childGo;
                        }
                    }
                } else if (name.IsS("Boar_Tusk")) {
                    boarTusk = atcs[i];
                } else if (name.IsS("Spots")) {
                    go.ChildShow(1, 2);
                } else {
                    GameObject modelGo = go.ChildGo(1, 0);
                    if (modelGo)
                        modelGo.RenMat(A.Load<Material>("PolyArtAnimal/" + type + "/Materials/" + name));
                }
            }
            if (boarTusk.tS().IsS("Boar_Tusk") && fangGo)
                fangGo.RenMat(A.Load<Material>("PolyArtAnimal/Boar/Materials/" + boarTusk));
            return go;
        }
        static GameObject ChildGo(GameObject a, string path) {
            int[] childs = path.IntArr();
            Transform tf = a.transform;
            for (int i = 0; i < childs.Length; i++) {
                if (tf.childCount > childs[i])
                    tf = tf.GetChild(childs[i]);
                else
                    return null;
            }
            return tf.gameObject;
        }
    }
}